#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Starte die Validierung einer Domain, wobei der Typ angegeben werden muss. Bitte beachten, dass die Validierung fuer die Hauptdomain durchgefuehrt werden muss, also z.B. fuer programmregister.de, nicht fuer www.programmregister.de")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Zu validierende Doamin')
    parser.add_argument('-l', '--login', dest='login', default = 'achtergarde_api', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    parser.add_argument('-t', '--type', dest='type', required=True, help='Der Typ der Validierung, cname, https  oder email')
    args = parser.parse_args()

    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'
    type = args.type
    if not type in { 'cname', 'email', 'https' }:
      print("Der Parameter type muss cname, mail oder https sein")
      exit(2) 

    Api = "https://cert-manager.com/api/dcv/v1/validation/start/domain/"+type
    my_headers = {'Content-Type': 'application/json;charset=utf-8', 'login': login, 'password': passphrase, 'customerUri': customerUri }
    body = {'domain': args.domain }
    response = requests.post(Api, headers=my_headers, json=body)
    jresp = json.loads(response.content)
    
    if type == "cname":
      print("Erstelle einen CNAME DNS Eintrag wie folgt:")
      print(jresp['host'] + " CNAME " + jresp['point'])
      print("")
      print("Fuehre danach das Skript 'SubmitValidation.py -t cname -d "+ domain + "' aus, um die Validierung abzuschließen.")

    elif type == "https": 
      print("Erstelle einen HTTP Eintrag wie folgt:")
      print("")
      print("Die URL " + jresp['url'] + " muss folgenden Inhalt haben:")
      print(jresp['firstLine'])
      print(jresp['secondLine'])
      print("")
      print("Fuehre danach das Skript 'SubmitValidation.py -d " + args.domain + " -t https' aus, um die Validierung abzuschließen.")

    else: 
      print("Verschicke die Validierungs-Email fuer die Domain " + domain + " bitte mit einem der folgenden Kommandos, abhaengig von der gewuenschten EMail-Adresse:")
      for mail in jresp['emails']:
          print("SubmitValidation.py -d " + domain + " -t email -e " + mail)


if __name__ == "__main__":
    main()

