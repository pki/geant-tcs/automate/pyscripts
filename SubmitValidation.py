#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Starte den Submit fuer die Validierung einer Domain per CNAME. Bitte beachten, dass der Submit fuer die Hauptdomain durchgefuehrt werden muss, also z.B. fuer programmregister.de, nicht fuer www.programmregister.de")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Zu validierende Doamin')
    parser.add_argument('-e', '--email', dest='email', help='Die zur Validierung genutzte Email. Diese muss unterhalb der Domain liegen. Nur bei type email vergeben')
    parser.add_argument('-l', '--login', dest='login', default = 'achtergarde_api', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    parser.add_argument('-t', '--type', dest='type', required=True, help='Der Typ der Validierung, cname, https  oder email')
    args = parser.parse_args()
    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    domain = args.domain
    customerUri = 'DFN'
    type = args.type
    if not type in { 'cname', 'email', 'https' }:
      print("Der Parameter type muss cname, mail oder https sein")
      exit(2) 

    Api = "https://cert-manager.com/api/dcv/v1/validation/submit/domain/" + type
    my_headers = {'Content-Type': 'application/json;charset=utf-8', 'login': login, 'password': passphrase, 'customerUri': customerUri }
    if type in { 'cname', 'https' }:
      body = {'domain': domain }
    else:
      body = {'domain': domain, 'email': email }

    response = requests.post(Api, headers=my_headers, json=body)
    print(response.text)

if __name__ == "__main__":
    main()

